### QWCore
modified:   Classes/QWCore/Constants.h

modified:   Classes/QWCore/Constants.m

modified:   Classes/QWCore/QWSdk.h

modified:   Classes/QWCore/QWSdk.m

### QWNetworkCore
new file:   Classes/QWNetworkCore/NoNotificationsViewController.h

new file:   Classes/QWNetworkCore/NoNotificationsViewController.m

new file:   Classes/QWNetworkCore/NoNotificationsViewController.xib

new file:   Classes/QWNetworkCore/NotificationCell.xib

new file:   Classes/QWNetworkCore/NotificationCellTableViewCell.h

new file:   Classes/QWNetworkCore/NotificationCellTableViewCell.m

new file:   Classes/QWNetworkCore/NotificationViewController.h

new file:   Classes/QWNetworkCore/NotificationViewController.m

new file:   Classes/QWNetworkCore/NotificationViewController.xib

new file:   Classes/QWNetworkCore/PaymentRequestViewController.h

new file:   Classes/QWNetworkCore/PaymentRequestViewController.m

new file:   Classes/QWNetworkCore/PaymentRequestViewController.xib

modified:   Classes/QWNetworkCore/QWPlacesContainer.m

modified:   Classes/QWNetworkCore/QWThankYouPaymentViewController.xib

### QWAssets
new file:   QWAssets/Home.png

new file:   QWAssets/Home@2x.png

new file:   QWAssets/bg_notification.png

new file:   QWAssets/bg_notification@2x.png

new file:   QWAssets/icon_ReceiptRequest.png

new file:   QWAssets/icon_nonotification.png

new file:   QWAssets/icon_nonotification@2x.png

new file:   QWAssets/notification.png

new file:   QWAssets/notification@2x.png

new file:   QWAssets/icon_failure.png

new file:   QWAssets/icon_failure@2x.png

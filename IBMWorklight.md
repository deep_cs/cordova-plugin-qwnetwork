### Introduction

This guide will help you to upgrade to the latest release of the QWNetwork Cordova plugin for iOS.

### Sample App

https://bitbucket.org/deep_cs/qwconsumer/

### Cleanup

First of all you should remove the files from prior version of this plugin. Here are a few easy steps to get you started.

* In Xcode open the project and navigate to the ```classes``` folder and remove all the QWNetwork specific files.
* Remove ```QWCore_ios.framework``` and ```SDWebImage.framework``` located inside Framework group.
* Remove QWNetwork specific assets from Resources folder.
* Now build the project and make sure everything works.

### Setup

#### Clone the plugin

```$ git clone https://deep_cs@bitbucket.org/deep_cs/cordova-plugin-qwnetwork.git```

* Drag and drop ```QWConsumer.h``` and ```QWConsumer.m``` inside the classes group. In the dialog box select copy items if needed.
* Drag and drop ```QWCore```, ```QWNetworkCore``` and  ```Lib```  folder inside classes.
* Drag and drop ```QWAssets``` folder inside Resources group.
* Drag and drop ```IDMPBLocalizations.bundle``` and ```IDMPhotoBrowser.bundle``` inside resources folder.

### Change build settings
* In the project navigator, select your project and also select the target where you want to add the linker flag. In the build settings tab search for ```other linker flag``` and add ```-lc++``` flag for debug, release and distribution phase.
![](http://s32.postimg.org/ile9sdzit/Slice_1.png )

* Now in the build settings tab search for ``` language - c++ ``` and change the values of C++ Language Dialect and C++ Standard library.
![](https://cloud.githubusercontent.com/assets/307819/4850496/550b9cfc-606a-11e4-9386-cb02d3e7d763.png )

* Now Select the “Build Phases” tab, Open the ```Link Binary With Libraries``` section. Click the Add button (+) and add the following frameworks.

    - ```MapKit.framework```
    - ```QuartzCore.framework```
    - ```CoreImage.framework```
    - ```CoreGraphics.framework```

    ![](http://s32.postimg.org/mfgq654kl/Screen_Shot_2016_05_02_at_3_30_06_AM.png)

From here, you can hit the ```Run``` button, or hit``` ⌘R ```on your keyboard and you’ll see the simulator pop up with the welcome screen.

#### Still need help?

We’re here to help. Get in touch and we’ll get back to you as soon as we can. [Contact us](mailto:support@livquik.com)

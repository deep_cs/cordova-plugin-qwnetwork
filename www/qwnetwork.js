/*global cordova, module*/

module.exports = {
    showPlaces: function (name, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "QWNetwork", "showPlaces", [name]);
    },
    launchQRPay: function (name, successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "QWNetwork", "launchQRPay", [name]);
    }
};

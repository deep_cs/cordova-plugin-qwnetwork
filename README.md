## Cordova QWNetwork Plugin

This plugin allows to add QuikWallet Payments to your application using QuikWallet Mobile SDK Native library

#### Clone the plugin

    $ git clone https://deep_cs@bitbucket.org/deep_cs/cordova-plugin-qwnetwork.git

#### Create a new Cordova Project

    $ cordova create demoApp com.example.demoapp demoApp

#### Install the plugin

    $ cd demoApp
    $ cordova plugin add ../cordova-plugin-QWNetwork


Edit `www/js/index.js` and add the following code inside `onDeviceReady`

```js
    var success = function(message) {
        console.log("Success");
    }

    var failure = function() {
        alert("Error calling QW_NETWORK Plugin");
    }

    var data = {
      "mobile":"9878765645",
      "signature": "55b0df1366992dac5b6510b4b2cbb2d40dd7750facbd8c56d412f6724d018481",
      "partnerid": "138",
      "transactionId": "T-XXXXXXXXX",
      "latitude" : "19.114105",
      "longitude": "72.893660"
    };
    // Launch place listing
    qwnetwork.showPlaces(data, success, failure);

    // Launch QuikPay 
    // qwnetwork.launchQRPay(data, success, failure);
    
```

#### Install iOS platform

    cordova platform add ios
    cordova build ios

### Change build settings
Enter ```open demoApp.xcodeproj/``` in your shell and XCode will automatically launch, opening the project.

* In the project navigator, select your project and also select the target where you want to add the linker flag. In the build settings tab search for ```other linker flag``` and add ```-lc++``` flag for debug, release and distribution phase.
![](http://s32.postimg.org/ile9sdzit/Slice_1.png )

* Now in the build settings tab search for ``` language - c++ ``` and change the values of C++ Language Dialect and C++ Standard library.
![](https://cloud.githubusercontent.com/assets/307819/4850496/550b9cfc-606a-11e4-9386-cb02d3e7d763.png )


#### Initialize QWSDK

In your AppDelegate.m file first import ```QWSdk.h``` file

    #import "QWSdk.h"

Optional - This step is needed only for testing your integration, for production environment this is not needed.

Add the following line to the start of ```didFinishLaunchingWithOptions```.
```
    - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    	[QuikWallet setDefaultConfig:@"test"]; // for testing purposes; not needed for live environment.

    	return YES;
    }
```
From here, you can hit the ```Run``` button, or hit``` ⌘R ```on your keyboard and you’ll see the simulator pop up with the welcome screen.

#### Still need help?

We’re here to help. Get in touch and we’ll get back to you as soon as we can. [Contact us](mailto:support@livquik.com)

#### TEST Credentials

```
TEST_MOBILE = 9878765645
SIGNATURE = 55b0df1366992dac5b6510b4b2cbb2d40dd7750facbd8c56d412f6724d018481
PARTNERID = 138
```

#### UAT Credentials

```
UAT_MOBILE = 9878765645
UAT_SIGNATURE = 796941efc3b883a8a65490e0c6b3b5b950bfe9c7ea201ce56e5b129356ce6c1f
UAT_PARTNERID = 156
```
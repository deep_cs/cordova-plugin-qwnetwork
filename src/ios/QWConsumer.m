#import "QWConsumer.h"
#import "QWPlacesContainer.h"
#import "QWQuikPayViewController.h"

@implementation QWConsumer
@synthesize callbackID;
- (void)showPlaces:(CDVInvokedUrlCommand*)command
{
    QWPlacesContainer *placesContainer = [[QWPlacesContainer alloc] init];
    self.callbackID = command.callbackId;
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:[[command arguments] objectAtIndex:0]];
    placesContainer.data = data;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:placesContainer];
    navigationController.navigationBar.barTintColor = [UIColor colorWithRed:78.0/255.0 green:157.0/255.0 blue:196.0/255.0 alpha:1.0];
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    navigationController.navigationBar.translucent = NO;
    [[navigationController navigationBar] setBarStyle:UIStatusBarStyleLightContent];
    [self.viewController presentViewController:navigationController animated:YES completion:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNotification:)
                                                 name:@"QWSdkDidTerminate_Notification"
                                            object:nil];
}

- (void)launchQRPay:(CDVInvokedUrlCommand*)command
{
    QWQuikPayViewController *quikpay = [[QWQuikPayViewController alloc] init];
    self.callbackID = command.callbackId;
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:[[command arguments] objectAtIndex:0]];
    quikpay.data = data;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:quikpay];
    navigationController.navigationBar.barTintColor = [UIColor colorWithRed:78.0/255.0 green:157.0/255.0 blue:196.0/255.0 alpha:1.0];
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    navigationController.navigationBar.translucent = NO;
    [[navigationController navigationBar] setBarStyle:UIStatusBarStyleLightContent];
    [self.viewController presentViewController:navigationController animated:YES completion:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivedNotification:)
                                                 name:@"QWSdkDidTerminate_Notification"
                                            object:nil];
}

- (void) returnControll{
    NSString* jsString = [NSString stringWithFormat:@"QWSdkDidTerminate()"];
    [self.webView stringByEvaluatingJavaScriptFromString:jsString];
}

- (void) receivedNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"QWSdkDidTerminate_Notification"]){
        NSLog (@"Successfully received the notification!");
        NSMutableDictionary* data = [NSMutableDictionary dictionaryWithCapacity:2];
        [data setObject:@"done" forKey: @"data"];
        CDVPluginResult *result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:data];
        [self.commandDelegate sendPluginResult:result callbackId:self.callbackID];
    }
}

@end

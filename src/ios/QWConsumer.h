#import <Cordova/CDV.h>

@interface QWConsumer : CDVPlugin

@property(strong) NSString* callbackID;

- (void) showPlaces:(CDVInvokedUrlCommand*)command;

- (void)launchQRPay:(CDVInvokedUrlCommand*)command;

@end

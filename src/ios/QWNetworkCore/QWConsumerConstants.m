//
//  QWConsumerConstants.m
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 23/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import "QWConsumerConstants.h"

@implementation QWConsumerConstants

//netbanking mappings
NSString * const NB_AXISBankNetbanking = @"AXIS Bank NetBanking";
NSString * const NB_BankOfIndia = @"Bank of India";
NSString * const NB_BankOfMaharashtra = @"Bank of Maharashtra";
NSString * const NB_CentralBankOfIndia = @"Central Bank Of India";
NSString * const NB_CorporationBank = @"Corporation Bank";
NSString * const NB_DevelopmentCreditBank = @"Development Credit Bank";
NSString * const NB_FederalBank = @"Federal Bank";
NSString * const NB_HDFCBank = @"HDFC Bank";
NSString * const NB_ICICINetbanking = @"ICICI Netbanking";
NSString * const NB_IndustrialDevelopmentBankOfIndia = @"Industrial Development Bank of India";
NSString * const NB_IndianBank= @"Indian Bank";
NSString * const NB_IndusIndBank = @"IndusInd Bank";
NSString * const NB_IndianOverseasBank = @"Indian Overseas Bank";
NSString * const NB_JammuAndKashmirBank = @"Jammu and Kashmir Bank";
NSString * const NB_KarnatakaBank = @"Karnataka Bank";
NSString * const NB_KarurVysya = @"Karur Vysya";
NSString * const NB_SBofBikanerAndjaipur = @"State Bank of Bikaner and Jaipur";
NSString * const NB_SBofHyderabad = @"State Bank of Hyderabad";
NSString * const NB_SBofIndia = @"State Bank of India";
NSString * const NB_SBofMysore = @"State Bank of Mysore";
NSString * const NB_SBofTravancore = @"State Bank of Travancore";
NSString * const NB_SouthIndianBank = @"South Indian Bank";
NSString * const NB_UnionBankOfIndia = @"Union Bank of India";
NSString * const NB_UnitedBankOfIndia = @"United Bank Of India";
NSString * const NB_VijayaBank = @"Vijaya Bank";
NSString * const NB_YesBank = @"Yes Bank";
NSString * const NB_CityUnion = @"CityUnion";
NSString * const NB_CanaraBank = @"Canara Bank";
NSString * const NB_SBOfPatiala = @"State Bank of Patiala";
NSString * const NB_CitiBankNetbanking = @"Citi Bank NetBanking";
NSString * const NB_DeutscheBank = @"Deutsche Bank";
NSString * const NB_KotakBank = @"Kotak Bank";

//Image stuff
NSString * const IMAGE_URL_PREFIX=@"http://d2msriaqamo9bm.cloudfront.net/";
NSString * const LOGOS=@"logos/ios/retina";
NSString * const BANNERS=@"bannersV2/ios/retina";
NSString * const MENU_IMAGES=@"menu";

@end

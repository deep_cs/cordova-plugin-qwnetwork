//
//  QWFeedbackViewController.m
//  QWConsumer
//
//  Created by Monideep Purkayastha on 20/04/16.
//
//

#import "QWFeedbackViewController.h"
#import "QWActivityHelper.h"
#import "QWPlacesContainer.h"
#import "QWSdk.h"

@interface QWFeedbackViewController ()

@end

@implementation QWFeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    self.navigationItem.title = @"FEEDBACK";
    
    [self registerForKeyboardNotifications];
    
    self.experienceRatingView.maximumValue = 5;
    self.experienceRatingView.minimumValue = 0;
    self.experienceRatingView.value = 0;
    [self.experienceRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    
    self.foodRatingView.maximumValue = 5;
    self.foodRatingView.minimumValue = 0;
    self.foodRatingView.value = 0;
    [self.foodRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    
    self.serviceRatingView.maximumValue = 5;
    self.serviceRatingView.minimumValue = 0;
    self.serviceRatingView.value = 0;
    [self.serviceRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    
    self.ambienceRatingView.maximumValue = 5;
    self.ambienceRatingView.minimumValue = 0;
    self.ambienceRatingView.value = 0;
    [self.ambienceRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    
    UIToolbar *keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    self.comments.inputAccessoryView = keyboardDoneButtonView;
    
    self.comments.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (IBAction)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void) popBack{
    if ([self.presentationStyle isEqualToString:@"presentViewController"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else{
        for (UIViewController *controller in self.navigationController.viewControllers) {
            if ([controller isKindOfClass:[QWPlacesContainer class]]) {
                [self.navigationController popToViewController:controller
                                                    animated:YES];
                break;
            }
        }
    }
}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    NSLog(@"Rating : %0.1f", sender.value);
}

- (IBAction)done:(id)sender{
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary *dict = @{@"foodratings":[NSString stringWithFormat:@"%0.1f", self.foodRatingView.value],
                           @"ambienceratings":[NSString stringWithFormat:@"%0.1f", self.ambienceRatingView.value],
                           @"servicesratings": [NSString stringWithFormat:@"%0.1f", self.serviceRatingView.value],
                           @"experienceratings":[NSString stringWithFormat:@"%0.1f", self.experienceRatingView.value],
                           @"comments":self.comments.text,
                           @"partnerid" : [self.data objectForKey:@"partnerid"],
                           @"mobile" : [self.data objectForKey:@"mobile"],
                           @"signature" : [self.data objectForKey:@"signature"],
                           @"transactionId": [self.data objectForKey:@"transactionId"]
                           };
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    if ( !IsEmpty( self.outLetId) ) {
        [data setObject: self.outLetId forKey: @"outletId"];
    }
    
    if ( !IsEmpty( self.retailerId)) {
        [data setObject: self.retailerId forKey: @"retailerId"];
    }
    
    void(^success)() = ^void(id  data){
        
        [QWActivityHelper removeActivityIndicator:self.view];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"FeedbackNotification"
         object:self];
    
        [self popBack];

    };
    
    void(^failure)() = ^void(NSError* error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [QWActivityHelper removeActivityIndicator:self.view];
        });
        
        NSDictionary *errorInfo = [error userInfo];
        
        if ([UIAlertController class]){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw feedback:data :success :failure];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if ([self.comments isFirstResponder]) {
        
        NSDictionary* info = [aNotification userInfo];
        CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
        CGSize kbSize =keyPadFrame.size;
        
        CGRect activeRect=[self.view convertRect:self.comments.frame fromView:self.comments.superview];
        CGRect aRect = self.view.bounds;
        aRect.size.height -= (kbSize.height);
        
        CGPoint origin =  activeRect.origin;
        origin.y -= self.scrollView.contentOffset.y;
        
        if (!CGRectContainsPoint(aRect, origin)) {
            CGPoint scrollPoint = CGPointMake(0.0,CGRectGetMaxY(activeRect)-(aRect.size.height));
            [self.scrollView setContentOffset:scrollPoint animated:NO];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    CGRect frame = self.scrollView.frame;
    self.scrollView.frame = frame;
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}


@end

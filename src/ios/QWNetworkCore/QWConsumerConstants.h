//
//  QWConsumerConstants.h
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 23/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QWConsumerConstants : NSObject

//netbanking mappings
FOUNDATION_EXPORT NSString * const NB_AXISBankNetbanking ;
FOUNDATION_EXPORT NSString * const NB_BankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_BankOfMaharashtra ;
FOUNDATION_EXPORT NSString * const NB_CentralBankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_CorporationBank ;
FOUNDATION_EXPORT NSString * const NB_DevelopmentCreditBank ;
FOUNDATION_EXPORT NSString * const NB_FederalBank ;
FOUNDATION_EXPORT NSString * const NB_HDFCBank ;
FOUNDATION_EXPORT NSString * const NB_ICICINetbanking ;
FOUNDATION_EXPORT NSString * const NB_IndustrialDevelopmentBankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_IndianBank;
FOUNDATION_EXPORT NSString * const NB_IndusIndBank;
FOUNDATION_EXPORT NSString * const NB_IndianOverseasBank ;
FOUNDATION_EXPORT NSString * const NB_JammuAndKashmirBank ;
FOUNDATION_EXPORT NSString * const NB_KarnatakaBank ;
FOUNDATION_EXPORT NSString * const NB_KarurVysya ;
FOUNDATION_EXPORT NSString * const NB_SBofBikanerAndjaipur ;
FOUNDATION_EXPORT NSString * const NB_SBofHyderabad ;
FOUNDATION_EXPORT NSString * const NB_SBofIndia ;
FOUNDATION_EXPORT NSString * const NB_SBofMysore ;
FOUNDATION_EXPORT NSString * const NB_SBofTravancore ;
FOUNDATION_EXPORT NSString * const NB_SouthIndianBank ;
FOUNDATION_EXPORT NSString * const NB_UnionBankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_UnitedBankOfIndia ;
FOUNDATION_EXPORT NSString * const NB_VijayaBank ;
FOUNDATION_EXPORT NSString * const NB_YesBank  ;
FOUNDATION_EXPORT NSString * const NB_CityUnion ;
FOUNDATION_EXPORT NSString * const NB_CanaraBank  ;
FOUNDATION_EXPORT NSString * const NB_SBOfPatiala ;
FOUNDATION_EXPORT NSString * const NB_CitiBankNetbanking ;
FOUNDATION_EXPORT NSString * const NB_DeutscheBank ;
FOUNDATION_EXPORT NSString * const NB_KotakBank ;


// notifications for end app to capture
#define QW_PAYMENT_SUCCESS_NOTIFICATION     @"qw_payment_success_notification"
#define QW_PAYMENT_FRAILURE_NOTIFICATION    @"qw_payment_failure_notification"
#define QW_PAYMENT_CANCEL_NOTIFICATION      @"qw_payment_cancel_notification"

// notification for recharge / SDK internal events
#define QW_RECHARGE_SUCCESS_NOTIFICATION     @"qw_recharge_success_notification"
#define QW_RECHARGE_FAILURE_NOTIFICATION    @"qw_recharge_failure_notification"
#define QW_RECHARGE_CANCEL_NOTIFICATION      @"qw_recharge_cancel_notification"


// test data
//#define TEST_MOBILE @"9740401023"
//#define TEST_PARTNER_ID @"111"
//#define TEST_SINGATURE @"829f0e3febef9485855ed795119b04d208004391993abb5f51929ae0c124420c"
//#define TEST_LATITUDE @"19.114105"
//#define TEST_LONGITUDE @"72.893660"
//#define TEST_USER_ID @"15"
//#define TEST_PASSPHRASE @"modqNGool+SBu24V"

//TEST credential for QW NETWORK
//#define TEST_MOBILE @"9878765645"
//#define TEST_PARTNER_ID @"138"
//#define TEST_SINGATURE @"55b0df1366992dac5b6510b4b2cbb2d40dd7750facbd8c56d412f6724d018481"
//#define TEST_LATITUDE @"19.114105"
//#define TEST_LONGITUDE @"72.893660"

// UAT credentials for QW NETWORK
#define TEST_MOBILE @"9878765645"
#define TEST_PARTNER_ID @"156"
#define TEST_SINGATURE @"796941efc3b883a8a65490e0c6b3b5b950bfe9c7ea201ce56e5b129356ce6c1f"
#define TEST_LATITUDE @"19.114105"
#define TEST_LONGITUDE @"72.893660"


// For setting intended action of end app .. payment or show card etc... from CLINET APP
#define INTENDED_ACTION_FROM_CLIENT @"intended_action_from_client"
#define INTENDED_ACTION_FROM_SDK    @"intended_action_from_sdk"
#define INTENDED_CLIENT_ACTION_CONDUCT_PAYMENT @"intended_client_action_conduct_payment"
#define INTENDEDN_CLIENT_ACTION_SHOW_CARDS @"intended_client_action_show_cards"

// For setting intended action within SDK .. prepaid recharge etc... from QW SDK
#define INTENDED_SDK_ACTION @"intended_sdk_action"
#define INTENDED_SDK_ACTION_PREPAID_RECHARGE @"intended_sdk_action_prepaid_recharge"
#define SDK_ACTION @"sdk_action"

//Image stuff
FOUNDATION_EXPORT NSString * const IMAGE_URL_PREFIX;
FOUNDATION_EXPORT NSString * const LOGOS;
FOUNDATION_EXPORT NSString * const BANNERS;
FOUNDATION_EXPORT NSString * const MENU_IMAGES;

@end

//
//  OfferDetailsViewController.m
//  QuikWallet
//
//  Created by Monideep Purkayastha on 02/02/16.
//  Copyright © 2016 LivQuik. All rights reserved.
//

#import "OfferDetailsViewController.h"


@interface OfferDetailsViewController ()

@end

@implementation OfferDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.0/255.0 green: 154.0/255.0 blue: 251.0/255.0 alpha:1.0]];
    self.showPlaceBtn.layer.borderColor= [[UIColor colorWithRed:78.0/255.0 green:157.0/255.0 blue:196.0/255.0 alpha:1.0] CGColor];
    
    self.navigationItem.title = @"OFFER DETAILS";
    
    self.statictnc.text=@" \u2022 Quikwallet reserves the right to change offers at its sole discretion.\n\n \u2022 Offers are subject to change.\n\n \u2022 Quikwallet decision will be final and binding. \n\n \u2022 These offers are run by Quikwallet and are non-transferable.\n\n \u2022 No substitution / exchange / redemption for an equivalent cash amount shall be given in any form. \n\n \u2022 Participation in the offers shall be treated as an unconditional acceptance of the user to the Terms and Condition.";
    
    self.ldesc.text = [self.offer objectForKey:@"ldesc"];
    self.ldesc.adjustsFontSizeToFitWidth = NO;
    self.ldesc.numberOfLines = 0;
    self.ldesc.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.sdesc.text = [self.offer objectForKey:@"sdesc"];
    self.sdesc.adjustsFontSizeToFitWidth = NO;
    self.sdesc.numberOfLines = 0;
    self.sdesc.lineBreakMode = NSLineBreakByWordWrapping;
    
    if (!IsEmpty([self.offer objectForKey:@"tnc"])) {
        self.termsText.text = [self.offer objectForKey:@"tnc"];
    }
    
    if (self.placeDetailsFlag || [[self.offer objectForKey:@"universal"] boolValue]) {
        self.showPlaceBtn.hidden = YES;
    } else{
        if (!IsEmpty([self.offer objectForKey:@"places"])) {
            self.showPlaceBtn.hidden = NO;
        }else{
            self.showPlaceBtn.hidden = YES;
        }
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void) popBack {
    [self.navigationController popViewControllerAnimated:YES];
}


static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

@end

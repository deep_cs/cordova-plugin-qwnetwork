//
//  QWFailureViewController.m
//  QWConsumerQWConsumerAppIphone
//
//  Created by Deep on 26/05/16.
//
//

#import "QWFailureViewController.h"
#import "QWPlacesContainer.h"

@interface QWFailureViewController ()

@end

@implementation QWFailureViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    self.navigationItem.title = @"PAYMENT FAILED";

    UIImage* image4 = [UIImage imageNamed:@"btn_back.png"];
    CGRect frameimg1 = CGRectMake(0, 0, image4.size.width, image4.size.height);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg1];
    [backButton setBackgroundImage:image4 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;

    NSLog(@"PRESENTATION %@", self.presentationStyle);

    self.msgLbl.text = self.msg;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(id)sender {
    [self popBack];
}

- (void) popBack{
    if ([self.presentationStyle isEqualToString:@"presentViewController"]) {
        UIViewController *vc = self.presentingViewController;
        while (vc.presentingViewController) {
            vc = vc.presentingViewController;
        }
        [vc dismissViewControllerAnimated:YES completion:NULL];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"QWSdkDidTerminate_Notification"
         object:self];
    } else{
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"QWSdkDidTerminate_Notification"
         object:self];
    }
}

@end

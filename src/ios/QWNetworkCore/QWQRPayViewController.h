//
//  QWQRPayViewController.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 08/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HowToPayViewController.h"

@interface QWQRPayViewController : UIViewController
@property(nonatomic, strong) NSDictionary *paymentObject;
@property(nonatomic, strong) NSDictionary *prepaidObject;
@property (nonatomic, strong) NSMutableArray *howtopayData;
@property (weak, nonatomic) IBOutlet UIView *howToPayView;
- (IBAction)pay:(id)sender;
@property(nonatomic, strong) HowToPayViewController *howtoPay;
@property (nonatomic, strong) NSMutableDictionary *data;
@property (nonatomic, copy) NSString * presentationStyle;

@end

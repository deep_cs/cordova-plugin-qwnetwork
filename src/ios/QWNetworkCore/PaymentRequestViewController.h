//
//  PaymentRequestViewController.h
//  QWConsumerQWConsumerAppIphone
//
//  Created by Deep on 13/06/16.
//
//

#import <UIKit/UIKit.h>

@interface PaymentRequestViewController : UIViewController
- (IBAction)rejectPayment:(id)sender;
- (IBAction)proceed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *rejectPaymentBtn;
@property (weak, nonatomic) IBOutlet UILabel *amountLbl;
@property (weak, nonatomic) IBOutlet UILabel *billNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *quikID;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *retailerNameLbl;

@property (nonatomic, strong) NSDictionary *paymentDetails;
@property (nonatomic, strong) NSMutableDictionary *data;
@property (nonatomic, strong) NSString *currentIndex;

@end

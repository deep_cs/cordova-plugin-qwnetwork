//
//  QWFailureViewController.h
//  QWConsumerQWConsumerAppIphone
//
//  Created by Deep on 26/05/16.
//
//

#import <UIKit/UIKit.h>

@interface QWFailureViewController : UIViewController
@property(nonatomic, copy) NSString * msg;
@property(nonatomic, copy) NSString * presentationStyle;

@property (weak, nonatomic) IBOutlet UILabel *msgLbl;

@property (weak, nonatomic) IBOutlet UIButton *doneBtn;
- (IBAction)doneButtonPressed:(id)sender;
@end

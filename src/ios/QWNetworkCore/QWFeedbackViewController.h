//
//  QWFeedbackViewController.h
//  QWConsumer
//
//  Created by Monideep Purkayastha on 20/04/16.
//
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface QWFeedbackViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet HCSStarRatingView *experienceRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *foodRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *serviceRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ambienceRatingView;

@property (nonatomic,assign) float experienceRating;
@property (nonatomic,assign) float foodRating;
@property (nonatomic,assign) float ambienceRating;
@property (nonatomic,assign) float serviceRating;

@property (strong,nonatomic) NSString *outLetId;
@property (strong,nonatomic) NSNumber *retailerId;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
- (IBAction)done:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *done;
@property (weak, nonatomic) IBOutlet UITextField *comments;
@property(nonatomic, copy) NSString * presentationStyle;
@property (nonatomic, strong) NSMutableDictionary *data;
@end

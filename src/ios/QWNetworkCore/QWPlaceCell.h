//
//  QWPlaceCell.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWPlaceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *retailername;
@property (weak, nonatomic) IBOutlet UIImageView *logo;
@property (weak, nonatomic) IBOutlet UILabel *outletname;
@property (weak, nonatomic) IBOutlet UILabel *distance;
@property (weak, nonatomic) IBOutlet UILabel *delivery;
@property (weak, nonatomic) IBOutlet UIImageView *offersView;
@end

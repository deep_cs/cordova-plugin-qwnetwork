//
//  NotificationCellTableViewCell.h
//  QuikWallet
//
//  Created by Gunendu Das on 23/06/14.
//  Copyright (c) 2014 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCellTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *notificationIcon;
@property (strong, nonatomic) IBOutlet UILabel *notificationLabel;

@end

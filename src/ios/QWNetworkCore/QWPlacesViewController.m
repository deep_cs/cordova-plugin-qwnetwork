//
//  QWPlacesViewController.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWPlacesViewController.h"
#import "QWPlaceCell.h"
#import "UIImageView+imageHelper.h"
#import "QWQuikPayViewController.h"
#import "QWPlaceDetailsViewController.h"
#import "QWAppHelper.h"
#import "QWNoPlacesViewController.h"

@interface QWPlacesViewController ()

@end

@implementation QWPlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark UITableView Implementation
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.places count] > 0) {
        self.tableView.backgroundView = nil;
        return [self.places count];
    } else {
        QWNoPlacesViewController *noPlaces = [[QWNoPlacesViewController alloc] init];
        noPlaces.view.frame = self.tableView.bounds;
        self.tableView.backgroundView = noPlaces.view;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;

    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *latitude = [self.data objectForKey:@"latitude"];

    static NSString *MyIdentifier = @"QWPlaceCell";

    // Load prepaid card cell
    QWPlaceCell * cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];

    if (cell == nil) {
        [tableView registerNib:[UINib nibWithNibName:@"QWPlaceCell" bundle:nil] forCellReuseIdentifier:MyIdentifier];
        cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    }

    NSDictionary *place = [self.places objectAtIndex:indexPath.section];
    NSString *retailerNameUppercase = [[place objectForKey:@"retailername"] uppercaseString];

    cell.retailername.text = retailerNameUppercase;
    cell.outletname.text = [place objectForKey:@"name"];
    cell.retailername.adjustsFontSizeToFitWidth = NO;
    cell.retailername.numberOfLines = 0;
    cell.retailername.lineBreakMode = NSLineBreakByWordWrapping;

    if ( [[place objectForKey:@"delivery"] isEqualToString: @"N"]) {
        cell.delivery.hidden = YES;
    }

    self.outLetId = [place objectForKey:@"id"];

    if (self.screenIndex == 0) {
        NSString *url = [NSString stringWithFormat:@"%@/%@.png",[QWAppHelper getLogoUrlPrefix],[place objectForKey:@"retailerid"]];
        [cell.logo qwSetImageWithURL:url :@"icon_default.png"];
        cell.logo.layer.borderColor= [[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0] CGColor];
    }

    NSString *keyvalue = [place objectForKey:@"distance"];
    CGFloat val = [keyvalue floatValue];
    NSString *distance = [NSString stringWithFormat:@"%.1f",val];
    NSString *distance1 = [distance stringByAppendingString:@" km"];

    if(!latitude){
        cell.distance.hidden = YES;
    }else{
        cell.distance.text = distance1;
    }

    if (self.screenIndex == 1) {
        cell.logo.hidden = YES;
        cell.retailername.frame = CGRectMake(cell.logo.frame.origin.x, cell.retailername.frame.origin.y, cell.retailername.frame.size.width + cell.logo.frame.size.width, cell.retailername.frame.size.height);

        cell.outletname.frame = CGRectMake(cell.logo.frame.origin.x, cell.outletname.frame.origin.y, cell.outletname.frame.size.width + cell.logo.frame.size.width, cell.outletname.frame.size.height);
        cell.delivery.frame = CGRectMake(cell.logo.frame.origin.x, cell.delivery.frame.origin.y, cell.delivery.frame.size.width + cell.logo.frame.size.width, cell.delivery.frame.size.height);
    }

    if ([[place objectForKey:@"offers"] count] <= 0) {
        cell.offersView.hidden = YES;
    } else {
        cell.offersView.hidden = NO;
    }

    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *place = [self.places objectAtIndex:indexPath.section];
    QWPlaceDetailsViewController *placeDetails = [[QWPlaceDetailsViewController alloc] init];
    placeDetails.currentPlace = place;
    placeDetails.data = self.data;
    [self.navigationController pushViewController:placeDetails animated:YES];
}


- (IBAction)quikPay:(id)sender {
    QWQuikPayViewController *quikpay = [[QWQuikPayViewController alloc] init];
    quikpay.data = self.data;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:quikpay];
    navigationController.navigationBar.barTintColor = [UIColor colorWithRed:78.0/255.0 green:157.0/255.0 blue:196.0/255.0 alpha:1.0];
    navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    navigationController.navigationBar.translucent = NO;
    [[navigationController navigationBar] setBarStyle:UIStatusBarStyleLightContent];
    [self presentViewController:navigationController animated:YES completion:nil];
}

@end

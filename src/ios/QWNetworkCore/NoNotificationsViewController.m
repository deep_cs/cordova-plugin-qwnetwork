//
//  NoNotificationsViewController.m
//  QuikWallet
//
//  Created by livquik on 02/07/14.
//  Copyright (c) 2014 LivQuik. All rights reserved.
//

#import "NoNotificationsViewController.h"

@interface NoNotificationsViewController ()

@end

@implementation NoNotificationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
-(void) popBack {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
@end

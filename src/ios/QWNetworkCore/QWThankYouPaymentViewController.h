//
//  QWThankYouPaymentViewController.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 11/04/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWThankYouPaymentViewController : UIViewController

- (IBAction)done:(id)sender;
- (IBAction)feedback:(id)sender;

@property(nonatomic, copy) NSString * presentationStyle;
@property (strong,nonatomic) NSString *outLetId;
@property (strong,nonatomic) NSNumber *retailerId;
@property (nonatomic, strong) NSMutableDictionary *data;
@end

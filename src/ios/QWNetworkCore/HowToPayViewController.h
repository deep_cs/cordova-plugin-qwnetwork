//
//  HowToPayViewController.h
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 20/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HowToPayViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, assign) Boolean fulfilled;
@property (nonatomic, copy) NSString* toPay;
@property (nonatomic, copy) NSString * partnerId;
@property(nonatomic, assign) Boolean netbankingFlag;
@property (nonatomic, assign) Boolean powrpayFlag;
@property (nonatomic, copy) NSString * payee;
@property (nonatomic, strong) NSArray * howToPayArray;

@property (nonatomic, strong) NSMutableArray *paymentTypes;

@property (weak, nonatomic) IBOutlet UITableView *howToPayTable;
@end

//
//  NotificationCellTableViewCell.m
//  QuikWallet
//
//  Created by Gunendu Das on 23/06/14.
//  Copyright (c) 2014 LivQuik. All rights reserved.
//

#import "NotificationCellTableViewCell.h"

@implementation NotificationCellTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        NSArray *nibArray = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

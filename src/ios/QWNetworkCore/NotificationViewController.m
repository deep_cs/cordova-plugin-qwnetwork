//
//  NotificationViewController.m
//  QWConsumerQWConsumerAppIphone
//
//  Created by Deep on 13/06/16.
//
//

#import "NotificationViewController.h"
#import "QWActivityHelper.h"
#import "QWSdk.h"
#import "NoNotificationsViewController.h"
#import "NotificationCellTableViewCell.h"
#import "PaymentRequestViewController.h"

@interface NotificationViewController ()

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"RejectPaymentNotification"
                                               object:nil];

    self.navigationItem.title = @"NOTIFICATIONS";

    self.tableView.dataSource = self;
    self.tableView.delegate = self;

    [self getNotifications];
}

- (void) getNotifications{
    // Display spinner
    [QWActivityHelper displayActivityIndicator:self.view];

    void(^success)() = ^void(id  data){
        [QWActivityHelper removeActivityIndicator:self.view];
        self.loadingFlag = false;
        self.notifications = [data objectForKey:@"pendingpayments"];
        [self.tableView reloadData];
    };

    void(^failure)() = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        self.loadingFlag = false;
        NSDictionary *errorInfo = [error userInfo];

        if ([UIAlertController class]){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];

            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    };

    QWSdk *qw = [[QWSdk alloc] init];
    [qw pendingPayments:self.data :success :failure];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.notifications count] > 0) {
        return 1;
        self.tableView.backgroundView = nil;
    } else {
        if (self.loadingFlag) {
            return 0;
        } else{
            NoNotificationsViewController *noNotification = [[NoNotificationsViewController alloc] initWithNibName:@"NoNotificationsViewController" bundle:nil];
            noNotification.view.frame = self.tableView.bounds;
            self.tableView.backgroundView = noNotification.view;
        }
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.notifications count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    NotificationCellTableViewCell *cell = (NotificationCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[NotificationCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }

    NSDictionary *notification = [self.notifications objectAtIndex:indexPath.row];
    cell.notificationLabel.text = [NSString stringWithFormat:@"Payment request at %@", [notification objectForKey:@"payee"]];

    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *paymentData = [[NSMutableDictionary alloc] initWithDictionary:[self.notifications objectAtIndex:indexPath.row]];
    PaymentRequestViewController *paymentrequest =[[PaymentRequestViewController alloc] init];
    paymentrequest.paymentDetails = paymentData;
    paymentrequest.data = self.data;
    paymentrequest.currentIndex = [NSString stringWithFormat:@"%lu", (long)indexPath.row];
    [self.navigationController pushViewController:paymentrequest animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void) receiveTestNotification:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"RejectPaymentNotification"]){

        NSDictionary *userInfo = notification.userInfo;
        NSString *currentIndex = [userInfo objectForKey:@"index"];

        NSMutableArray *mutableArray = [self.notifications mutableCopy];
        NSMutableArray *discardedItems = [NSMutableArray array];

        [discardedItems addObject:[self.notifications objectAtIndex:[currentIndex intValue]]];

        [mutableArray removeObjectsInArray:discardedItems];
        self.notifications = mutableArray;

        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[currentIndex intValue] inSection:0];
        NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];

        if(![self.notifications count]){
            [indexes addIndex: indexPath.section];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView deleteSections:indexes withRowAnimation:UITableViewRowAnimationFade];
            });

        } else{
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            });

        }

    }
}


@end

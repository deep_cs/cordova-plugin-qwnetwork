//
//  NotificationViewController.h
//  QWConsumerQWConsumerAppIphone
//
//  Created by Deep on 13/06/16.
//
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic, strong) NSMutableDictionary *data;
@property(nonatomic, strong) NSMutableArray *notifications;
@property(nonatomic, assign) Boolean loadingFlag;
@end

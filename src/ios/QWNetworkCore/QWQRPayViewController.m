//
//  QWQRPayViewController.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 08/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWQRPayViewController.h"
#import "QWSdk.h"
#import "QWThankYouPaymentViewController.h"
#import "QWActivityHelper.h"
#import "QWFailureViewController.h"

@interface QWQRPayViewController ()

@end

@implementation QWQRPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
     self.navigationItem.title = [self.paymentObject objectForKey: @"payee"];

    self.howtoPay = [[HowToPayViewController alloc] init];
    self.howtoPay.toPay = [self.paymentObject objectForKey:@"amount"];
    self.howtoPay.howToPayArray = self.howtopayData;
    [self addChildViewController:self.howtoPay];
    [self.howtoPay didMoveToParentViewController:self];
    [self.howtoPay.view setFrame:self.howToPayView.frame];
    [self.howToPayView addSubview:self.howtoPay.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)pay:(id)sender {
    
    [QWActivityHelper displayActivityIndicator:self.view];
    
    void(^success)() = ^void(id  data){
        
        [QWActivityHelper removeActivityIndicator:self.view];
                
        QWThankYouPaymentViewController *thankYou = [[QWThankYouPaymentViewController alloc] init];
        thankYou.presentationStyle = self.presentationStyle;
        thankYou.outLetId = [self.paymentObject objectForKey:@"outletid"];
        thankYou.data = self.data;
        [self.navigationController pushViewController:thankYou animated:YES];
    };
    
    void(^failure)() = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        NSDictionary *errorInfo = [error userInfo];
        NSString *message  = [errorInfo objectForKey:@"NSLocalizedDescription"];
        
        QWFailureViewController *failure = [[QWFailureViewController alloc] init];
        failure.msg = message;
        failure.presentationStyle = self.presentationStyle;
        [self.navigationController pushViewController:failure animated:YES];
        
//        if ([UIAlertController class]){
//            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
//                                                                           message:[errorInfo objectForKey:@"NSLocalizedDescription"]
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                                  handler:^(UIAlertAction * action) {}];
//            
//            [alert addAction:defaultAction];
//            [self presentViewController:alert animated:YES completion:^{
//                
//                NSArray *viewControllers = [NSArray arrayWithArray:self.navigationController.viewControllers];
//                
//                NSArray *whitelist = [NSArray arrayWithObjects:@"QWPlaceDetailsViewController", @"QWQuikPayViewController",@"QWPaymentRequestController",nil];
//                
//                for (int i= (int)[viewControllers count] - 1; i >= 0; i--) {
//                    NSString *controllerName = NSStringFromClass([viewControllers[i] class]);
//                    for (NSString *vcName in whitelist) {
//                        
//                        if ([vcName isEqualToString:controllerName]) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [self.navigationController popToViewController:viewControllers[i] animated:YES];
//                            });
//                            break;
//                        }
//                    }
//                }
//                
//            }];
//
//        } else{
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
//                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//            
//            NSArray *viewControllers = [NSArray arrayWithArray:self.navigationController.viewControllers];
//            
//            NSArray *whitelist = [NSArray arrayWithObjects:@"QWPlaceDetailsViewController", @"QWQuikPayViewController",@"QWPaymentRequestController",nil];
//            
//            for (int i= (int)[viewControllers count] - 1; i >= 0; i--) {
//                NSString *controllerName = NSStringFromClass([viewControllers[i] class]);
//                for (NSString *vcName in whitelist) {
//                    
//                    if ([vcName isEqualToString:controllerName]) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [self.navigationController popToViewController:viewControllers[i] animated:YES];
//                        });
//                        break;
//                    }
//                }
//            }
//            
//
//        }

    };
    
    NSDictionary *dict = @{
                           @"id":[self.paymentObject objectForKey:@"id"],
                           @"partnerid" : [self.data objectForKey:@"partnerid"],
                           @"mobile" : [self.data objectForKey:@"mobile"],
                           @"signature" : [self.data objectForKey:@"signature"],
                           @"transactionId": [self.data objectForKey:@"transactionId"],
                           };
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary: dict];
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw paymentFulfilledRequest:data :success :failure];

}
@end

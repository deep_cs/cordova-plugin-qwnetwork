//
//  QWAppHelper.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QWAppHelper : NSObject
+(NSString *)getLogoUrlPrefix;
+(NSString *)getBannersUrlPrefix;
+(NSString *)getMenuUrlPrefix;
@end

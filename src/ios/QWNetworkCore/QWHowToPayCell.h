//
//  QWHowToPayCell.h
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 23/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWHowToPayCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *redemptionLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountLbl;

@end

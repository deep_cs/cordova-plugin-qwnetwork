//
//  QWPayNowViewController.m
//  QWCore_ios_CONSUMER
//
//  Created by Monideep Purkayastha on 20/11/15.
//  Copyright © 2015 LivQuik. All rights reserved.
//

#import "QWPayNowViewController.h"
#import "QWActivityHelper.h"
#import "HowToPayViewController.h"
#import "QWOnGoingPaymentViewController.h"
#import "QWConsumerConstants.h"
#import "QWThankYouPaymentViewController.h"
#import "QWSdk.h"
#import "QWFailureViewController.h"

@interface QWPayNowViewController ()

@end

@implementation QWPayNowViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;

    self.navigationItem.title = @"PAYMENT DETAILS";

    self.grandTotalLbl.text = [NSString stringWithFormat:@"₹%@", [self.data objectForKey:@"amount"]];

    [self setUphowToPay];

}

- (void) setUphowToPay{
    // Display spinner
    [QWActivityHelper displayActivityIndicator:self.view];

    void(^success)() = ^void(id  data){
        self.paymentData = data;

        [QWActivityHelper removeActivityIndicator:self.view];

        NSInteger howtopaycount = [[data objectForKey:@"howtopay"] count];

        if (!IsEmpty([data objectForKey:@"message"])) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[data objectForKey:@"message"]
                                                            message: @""
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }

        if(howtopaycount> 0)
        {
            self.howToPay = [[HowToPayViewController alloc] init];

            self.howToPay.fulfilled = [[data objectForKey:@"fulfilled"] boolValue];
            self.howToPay.toPay = [data objectForKey:@"topay"];
            self.howToPay.partnerId = [data objectForKey:@"partnerid"];
            self.howToPay.netbankingFlag = [[data objectForKey:@"netbanking"] boolValue];
            self.howToPay.powrpayFlag = [[data objectForKey:@"powrpay"] boolValue];
            self.howToPay.payee = [data objectForKey:@"payee"];
            self.howToPay.howToPayArray = [data objectForKey:@"howtopay"];

            CGRect howToPayFrame = self.howToPayView.frame;
            howToPayFrame.origin.y = 0;
            self.howToPay.view.frame = howToPayFrame;

            [self.howToPay didMoveToParentViewController:self];
            [self.howToPayView addSubview:self.howToPay.view];
        }
        else
        {
            // if howtopay isnt returned anything then assume full payment hs to happen and proceed to cards view .

        }

    };

    void(^failure)() = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];

        NSDictionary *errorInfo = [error userInfo];
        NSString * message = [errorInfo objectForKey:@"NSLocalizedDescription"];
        
        QWFailureViewController *failure = [[QWFailureViewController alloc] init];
        failure.msg = message;
        failure.presentationStyle = self.presentationStyle;
        [self.navigationController pushViewController:failure animated:YES];
        
//        if ([UIAlertController class]) {
//            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
//                                                                           message:message
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                                  handler:^(UIAlertAction * action) {}];
//            
//            [alert addAction:defaultAction];
//            [self presentViewController:alert animated:YES completion:^{
//                NSArray *viewControllers = [NSArray arrayWithArray:self.navigationController.viewControllers];
//                
//                NSArray *whitelist = [NSArray arrayWithObjects:@"QWPlaceDetailsViewController", @"QWQuikPayViewController",@"QWPaymentRequestController",nil];
//                
//                for (int i= (int)[viewControllers count] - 1; i >= 0; i--) {
//                    NSString *controllerName = NSStringFromClass([viewControllers[i] class]);
//                    for (NSString *vcName in whitelist) {
//                        
//                        if ([vcName isEqualToString:controllerName]) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [self.navigationController popToViewController:viewControllers[i] animated:YES];
//                            });
//                            break;
//                        }
//                    }
//                }
//            }];
//
//        } else{
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
//                                                            message:message
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//            
//            NSArray *viewControllers = [NSArray arrayWithArray:self.navigationController.viewControllers];
//            
//            NSArray *whitelist = [NSArray arrayWithObjects:@"QWPlaceDetailsViewController", @"QWQuikPayViewController",@"QWPaymentRequestController",nil];
//            
//            for (int i= (int)[viewControllers count] - 1; i >= 0; i--) {
//                NSString *controllerName = NSStringFromClass([viewControllers[i] class]);
//                for (NSString *vcName in whitelist) {
//                    
//                    if ([vcName isEqualToString:controllerName]) {
//                        dispatch_async(dispatch_get_main_queue(), ^{
//                            [self.navigationController popToViewController:viewControllers[i] animated:YES];
//                        });
//                        break;
//                    }
//                }
//            }
//
//        }
    };


    QWSdk *qw = [[QWSdk alloc] init];
    [qw findModesOfPayment:self.data :success :failure];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}


- (IBAction)proceedToPayment:(id)sender {
    [QWActivityHelper displayActivityIndicator:self.view];

    if([[self.paymentData objectForKey:@"fulfilled"] boolValue]){

        NSDictionary *dict = @{
            @"amount":[self.data objectForKey:@"amount"],
            @"partnerid" : [self.data objectForKey:@"partnerid"],
            @"mobile" : [self.data objectForKey:@"mobile"],
            @"signature" : [self.data objectForKey:@"signature"],
            @"transactionId": [self.data objectForKey:@"transactionId"]
        };

        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];

        if (!IsEmpty([self.paymentData objectForKey:@"partnerid"])) {
            [data setObject:[self.paymentData objectForKey:@"partnerid"] forKey:@"partnerid"];
        }

        if (!IsEmpty([self.data objectForKey:@"outletid"])) {
            [data setObject:[self.data objectForKey:@"outletid"] forKey:@"outletid"];
        }
        
        if (!IsEmpty([self.data objectForKey:@"id"])) {
            [data setObject:[self.data objectForKey:@"id"] forKey:@"id"];
        }

        void(^success)() = ^void(id  data){

            [QWActivityHelper removeActivityIndicator:self.view];

            if ([[self.data objectForKey:SDK_ACTION] isEqualToString:INTENDED_ACTION_FROM_CLIENT]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:QW_PAYMENT_SUCCESS_NOTIFICATION  object:self userInfo:data];
            } else{
                QWThankYouPaymentViewController *thankYou = [[QWThankYouPaymentViewController alloc] init];
                thankYou.presentationStyle = self.presentationStyle;
                thankYou.outLetId = [self.data objectForKey:@"outletid"];
                thankYou.data = self.data;
                [self.navigationController pushViewController:thankYou animated:YES];
            }
        };

        void(^failure)() = ^void(NSError* error) {
            [QWActivityHelper removeActivityIndicator:self.view];

            NSDictionary *errorInfo = [error userInfo];
            NSString *message = [errorInfo objectForKey:@"NSLocalizedDescription"];
            
            QWFailureViewController *failure = [[QWFailureViewController alloc] init];
            failure.msg = message;
            failure.presentationStyle = self.presentationStyle;
            [self.navigationController pushViewController:failure animated:YES];
            
            
//            if([UIAlertController class]){
//                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
//                                                                               message:[errorInfo objectForKey:@"NSLocalizedDescription"]
//                                                                        preferredStyle:UIAlertControllerStyleAlert];
//                
//                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                                      handler:^(UIAlertAction * action) {}];
//                
//                [alert addAction:defaultAction];
//                [self presentViewController:alert animated:YES completion:^{
//                    
//                    NSArray *viewControllers = [NSArray arrayWithArray:self.navigationController.viewControllers];
//                    
//                    NSArray *whitelist = [NSArray arrayWithObjects:@"QWPlaceDetailsViewController", @"QWQuikPayViewController",@"QWPaymentRequestController",nil];
//                    
//                    for (int i= (int)[viewControllers count] - 1; i >= 0; i--) {
//                        NSString *controllerName = NSStringFromClass([viewControllers[i] class]);
//                        for (NSString *vcName in whitelist) {
//                            
//                            if ([vcName isEqualToString:controllerName]) {
//                                dispatch_async(dispatch_get_main_queue(), ^{
//                                    [self.navigationController popToViewController:viewControllers[i] animated:YES];
//                                });
//                                break;
//                            }
//                        }
//                    }
//                    
//                }];
//
//            } else{
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
//                                                                message:[errorInfo objectForKey:@"NSLocalizedDescription"]
//                                                               delegate:nil
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//                [alert show];
//                
//                NSArray *viewControllers = [NSArray arrayWithArray:self.navigationController.viewControllers];
//                
//                NSArray *whitelist = [NSArray arrayWithObjects:@"QWPlaceDetailsViewController", @"QWQuikPayViewController",@"QWPaymentRequestController",nil];
//                
//                for (int i= (int)[viewControllers count] - 1; i >= 0; i--) {
//                    NSString *controllerName = NSStringFromClass([viewControllers[i] class]);
//                    for (NSString *vcName in whitelist) {
//                        
//                        if ([vcName isEqualToString:controllerName]) {
//                            dispatch_async(dispatch_get_main_queue(), ^{
//                                [self.navigationController popToViewController:viewControllers[i] animated:YES];
//                            });
//                            break;
//                        }
//                    }
//                }
//            }

        };


        QWSdk *qw = [[QWSdk alloc] init];
        [qw paymentFulfilledRequest:data :success :failure];

        QWOnGoingPaymentViewController *ongoing = [[QWOnGoingPaymentViewController alloc] init];
        [self.navigationController pushViewController:ongoing animated:NO];
    }
    else
    {
        // Handle non fulfilled Payments
    }

}

@end

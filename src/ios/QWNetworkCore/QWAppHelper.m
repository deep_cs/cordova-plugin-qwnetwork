//
//  QWAppHelper.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWAppHelper.h"
#import "QWConsumerConstants.h"

@implementation QWAppHelper

+(NSString *)getLogoUrlPrefix
{
    return [NSString stringWithFormat:@"%@%@",IMAGE_URL_PREFIX,LOGOS];
}

+(NSString *)getBannersUrlPrefix
{
    return [NSString stringWithFormat:@"%@%@",IMAGE_URL_PREFIX,BANNERS];
}

+(NSString *)getMenuUrlPrefix
{
    return [NSString stringWithFormat:@"%@%@",IMAGE_URL_PREFIX,MENU_IMAGES];
}

@end

//
//  OffersAlertView.h
//  QuikWallet
//
//  Created by Monideep Purkayastha on 02/02/16.
//  Copyright © 2016 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OffersAlertView : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *offersTable;
@property (nonatomic, copy) NSMutableArray * offers;
@property (weak, nonatomic) UINavigationController * navigation;
@end

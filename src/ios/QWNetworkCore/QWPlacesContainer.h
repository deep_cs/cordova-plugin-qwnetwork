//
//  QWPlacesContainer.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QWPlacesContainer : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>
@property(nonatomic, strong) NSMutableDictionary *data;
@property (strong, nonatomic) UIPageViewController *pageView;
@property (nonatomic, copy) NSArray *places;
@property (nonatomic, copy) NSArray *retails;
@property (nonatomic, copy) NSArray *fnbs;
@property (nonatomic, copy) NSArray *offers;
@end

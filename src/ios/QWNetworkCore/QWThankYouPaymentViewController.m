//
//  QWThankYouPaymentViewController.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 11/04/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWThankYouPaymentViewController.h"
#import "QWPlacesContainer.h"
#import "QWFeedbackViewController.h"

@interface QWThankYouPaymentViewController ()

@end

@implementation QWThankYouPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    self.navigationItem.title = @"THANK YOU";
    
    UIImage* image4 = [UIImage imageNamed:@"Back-50.png"];
    CGRect frameimg1 = CGRectMake(0, 0, image4.size.width/2, image4.size.height/2);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg1];
    [backButton setBackgroundImage:image4 forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) popBack{
    if ([self.presentationStyle isEqualToString:@"presentViewController"]) {
        UIViewController *vc = self.presentingViewController;
        while (vc.presentingViewController) {
            vc = vc.presentingViewController;
        }
        [vc dismissViewControllerAnimated:YES completion:NULL];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"QWSdkDidTerminate_Notification"
         object:self];

    } else{
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"QWSdkDidTerminate_Notification"
         object:self];
    }
}

- (IBAction)done:(id)sender {
    [self popBack];
}
- (IBAction)feedback:(id)sender {
    QWFeedbackViewController *feedback = [[QWFeedbackViewController alloc] init];
    feedback.presentationStyle = self.presentationStyle;
    feedback.outLetId = self.outLetId;
    feedback.data = self.data;
    [self.navigationController pushViewController:feedback animated:YES];
}
@end

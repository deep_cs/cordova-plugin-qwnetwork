//
//  PaymentRequestViewController.m
//  QWConsumerQWConsumerAppIphone
//
//  Created by Deep on 13/06/16.
//
//

#import "PaymentRequestViewController.h"
#import "QWSdk.h"
#import "QWActivityHelper.h"
#import "QWPayNowViewController.h"
#import "QWConsumerConstants.h"

@interface PaymentRequestViewController ()

@end

@implementation PaymentRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    self.navigationItem.title = [[self.paymentDetails objectForKey:@"retailername"] uppercaseString];
    
    self.rejectPaymentBtn.layer.borderColor= [[UIColor colorWithRed:182.0/255.0 green:182.0/255.0 blue:182.0/255.0 alpha:1.0] CGColor];
    
    NSString *mask = @"₹ ";
    NSString *amountinstring = [NSString stringWithFormat:@"%@",[self.paymentDetails objectForKey:@"amount"]];
    self.amountLbl.text = [mask stringByAppendingString:amountinstring];
    
    if (!IsEmpty([self.paymentDetails objectForKey:@"billnumbers"])) {
        self.billNoLbl.text = [NSString stringWithFormat:@"Bill No. : %@",[self.paymentDetails objectForKey:@"billnumbers"]];
    } else {
        self.billNoLbl.text = @"Bill No.: NA" ;
    }
    
    self.retailerNameLbl.text = [self.paymentDetails objectForKey:@"outletname"];
    
    NSString *timeStampString = [self.paymentDetails objectForKey:@"timestamp"];
    NSTimeInterval _interval=[timeStampString doubleValue]/1000;
    NSDate *date1 = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setDateFormat:@"MMM dd, yyyy hh:mm"];
    NSString *_date=[_formatter stringFromDate:date1];
    
    NSString *transID = [NSString stringWithFormat:@"Quik ID: M-%@",[self.paymentDetails objectForKey:@"id"]];
    self.quikID.text = transID;
    self.timeLbl.text = _date;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)rejectPayment:(id)sender {
    
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSDictionary *dict = @{ @"amount": [self.paymentDetails objectForKey:@"amount"],
                            @"id": [self.paymentDetails objectForKey:@"id"],
                            @"outletid": [self.paymentDetails objectForKey:@"outletid"],
                            @"retailerid": [self.paymentDetails objectForKey:@"retailerid"],
                            @"type": @"pull",
                            @"partnerid" : [self.data objectForKey:@"partnerid"],
                            @"mobile" : [self.data objectForKey:@"mobile"],
                            @"signature" : [self.data objectForKey:@"signature"],
                            @"transactionId": [self.data objectForKey:@"transactionId"]
                           };
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    void(^success)() = ^void(id  data){
        [QWActivityHelper removeActivityIndicator:self.view];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Payment request cancelled"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:self.currentIndex forKey:@"index"];
        [[NSNotificationCenter defaultCenter] postNotificationName: @"RejectPaymentNotification" object:nil userInfo:userInfo];

        [self.navigationController popViewControllerAnimated:YES];
    };
    
    void(^failure)() = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        NSDictionary *errorInfo = [error userInfo];
        
        if ([UIAlertController class]){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    };
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw rejectPayment:data :success :failure];
    
}

- (IBAction)proceed:(id)sender {
    
    QWPayNowViewController *pay = [[QWPayNowViewController alloc] init];
    
    [self.data setObject:[self.paymentDetails objectForKey:@"amount"] forKey:@"amount"];
    [self.data setObject:[self.paymentDetails objectForKey:@"outletid"] forKey:@"outletid"];
    [self.data setObject:[self.paymentDetails objectForKey:@"retailerid"] forKey:@"retailerid"];
    [self.data setObject:[self.paymentDetails objectForKey:@"id"] forKey:@"id"];
    
    pay.data = self.data;
    [pay.data setObject:SDK_ACTION forKey:INTENDED_ACTION_FROM_SDK];
    [self.navigationController pushViewController:pay animated:YES];

}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

@end

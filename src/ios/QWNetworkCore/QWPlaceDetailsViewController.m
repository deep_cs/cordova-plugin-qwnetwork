//
//  QWPlaceDetailsViewController.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 12/04/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWPlaceDetailsViewController.h"
#import "QWReportIssueViewController.h"
#import "QWSdk.h"
#import "QWActivityHelper.h"
#import "QWAppHelper.h"
#import "UIImageView+imageHelper.h"
#import "NSString+FormValidation.h"
#import "QWPayNowViewController.h"
#import "QWConsumerConstants.h"
#import "GlobalSingleton.h"
#import "OffersAlertView.h"
#import <MapKit/MapKit.h>
#import "QWFeedbackViewController.h"


@interface QWPlaceDetailsViewController ()

@end

@implementation QWPlaceDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    [self registerForKeyboardNotifications];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popBack:)];
    [self.backBtn addGestureRecognizer:tapGesture];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStylePlain target:self
                                                                  action:@selector(doneClicked:)];
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    self.amountText.inputAccessoryView = keyboardDoneButtonView;
    
    self.outLetId = [self.currentPlace objectForKey:@"id"];
    self.retailerid = [self.currentPlace objectForKey:@"retailerid"];
    self.companyid = [self.currentPlace objectForKey:@"companyid"];
    self.retailername = [self.currentPlace objectForKey:@"retailername"];
    
    if([self.currentPlace valueForKey:@"phone"]!=nil) {
        self.phonenumber = [self.currentPlace objectForKey:@"phone"];
    }
    
    
    // Outlet logo
    NSString *logoUrl = [NSString stringWithFormat:@"%@/%@.png",[QWAppHelper getLogoUrlPrefix],[self.currentPlace objectForKey:@"retailerid"]];
    [self.logoImg qwSetImageWithURL:logoUrl :@"icon_default.png"];
    
    self.logoImg.contentMode = UIViewContentModeScaleAspectFit;
    self.logoImg.layer.borderColor= [[UIColor colorWithRed:230.0/255.0 green:230.0/255.0 blue:230.0/255.0 alpha:1.0] CGColor];
    
    self.retailerNameLabel.text = [[self.currentPlace objectForKey:@"retailername"] uppercaseString];
    self.outletNameLabel.text = [self.currentPlace objectForKey:@"name"];
    
    
    for (UIImageView *imageView in self.callBtns) {
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(callBtnPressed:)];
        [imageView addGestureRecognizer:tapGesture1];
    }
    
    for (UIImageView *imageView in self.addressBtns) {
        UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addressBtnPressed:)];
        [imageView addGestureRecognizer:tapGesture2];
    }
    
    for (UIImageView *imageView in self.menuBtns) {
        UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(menuBtnPressed:)];
        [imageView addGestureRecognizer:tapGesture3];
    }
    
    for (UIImageView *imageView in self.offerBtns) {
        UITapGestureRecognizer *tapGesture4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(offerBtnPressed:)];
        [imageView addGestureRecognizer:tapGesture4];
    }

    // Banner Image
    NSString *bannerImgUrl = [NSString stringWithFormat:@"%@/%@.png",[QWAppHelper getBannersUrlPrefix], [self.currentPlace objectForKey:@"retailerid"]];
    
    if ([[self.currentPlace objectForKey:@"categoryid"] intValue] == 1) {
        [self.retailerLogo qwSetImageWithURL:bannerImgUrl :@"default_banner_retail.png"];
    } else{
        [self.retailerLogo qwSetImageWithURL:bannerImgUrl :@"default_banner_fnb.png"];
    }
    
    if ([[self.currentPlace objectForKey:@"checkedin"] isEqualToString:@"Y"]) {
        self.checkinflag = true;
        [self.checkinBtn setTitle:@"CHECK OUT" forState:UIControlStateNormal];
    } else{
        self.checkinflag = false;
        [self.checkinBtn setTitle:@"CHECK IN" forState:UIControlStateNormal];
    }
    
    self.menuCount = [[self.currentPlace objectForKey:@"menucount"] integerValue];
    self.offersCount = [[self.currentPlace objectForKey:@"offers"] count];
     
    // Crap code - Need to be rewritten with autolayout
    // So that we don't have to adjust the frames for each senarios.
    
    if (self.menuCount > 0 && self.offersCount > 0) {
        self.withoutMenuAndOfferBtn.hidden = YES;
        self.withoutMenuBtn.hidden = YES;
        self.withoutOffersBtn.hidden = YES;
        self.allActionBtns.hidden = NO;
    }else if (self.menuCount == 0 && self.offersCount == 0) {
        self.withoutMenuAndOfferBtn.hidden = NO;
        self.withoutMenuBtn.hidden = YES;
        self.withoutOffersBtn.hidden = YES;
        self.allActionBtns.hidden = YES;
    } else if (self.menuCount == 0){
        self.withoutMenuBtn.hidden = NO;
        self.allActionBtns.hidden = YES;
        self.withoutMenuAndOfferBtn.hidden = YES;
        self.withoutOffersBtn.hidden = YES;
        
    }else if (self.offersCount == 0){
        self.withoutOffersBtn.hidden = NO;
        self.allActionBtns.hidden = YES;
        self.withoutMenuAndOfferBtn.hidden = YES;
        self.withoutMenuBtn.hidden = YES;
    }

    if(self.menuCount >0)
    {
        NSMutableArray *menuData = [[NSMutableArray alloc] init];
        
        for (int i=1; i <= self.menuCount; i++) {
            [menuData addObject: [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@/%d.jpg",[QWAppHelper getMenuUrlPrefix],self.outLetId, i]] ];
        }
        
        self.menuImages = [[NSArray alloc] initWithObjects: menuData, nil];
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)dismissKeyboard {
    [self.view endEditing:YES];
}

- (void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

-(void) popBack: (id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)checkin:(id)sender {
    
    [QWActivityHelper displayActivityIndicator:self.view];
    
    if(!self.checkinflag)
    {
        void(^success)() = ^void(id  data){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [QWActivityHelper removeActivityIndicator:self.view];
            });
            
            if ([data objectForKey:@"otp"] != nil) {
                NSString *otp = [NSString stringWithFormat:@"%@", [data objectForKey:@"otp"]];
                
                if ([UIAlertController class]){
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:otp
                                                                                   message:@"Provide this OTP to your attendant"
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {}];
                    
                    [alert addAction:defaultAction];
                    [self presentViewController:alert animated:YES completion:nil];

                } else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:otp
                                                                    message: @"Provide this OTP to your attendant"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
            }
            
            self.checkinflag = true;
            [self.checkinBtn setTitle:@"CHECK OUT" forState:UIControlStateNormal];
        };
        
        void(^failure)() = ^void(NSError* error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [QWActivityHelper removeActivityIndicator:self.view];
            });
            
            NSDictionary *errorInfo = [error userInfo];
            
            
            if ([UIAlertController class]){
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                               message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            } else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        };
        
        NSDictionary *dict = @{@"partnerid" : [self.data objectForKey:@"partnerid"],
                               @"mobile" : [self.data objectForKey:@"mobile"],
                               @"signature" : [self.data objectForKey:@"signature"],
                               @"transactionId": [self.data objectForKey:@"transactionId"],
                               @"outletid":self.outLetId,
                               @"retailerid":self.retailerid,
                               @"type":@"app"
                               };
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw checkin:data :success :failure];
    }
    
    else
    {
        void(^success)() = ^void(id  data){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [QWActivityHelper removeActivityIndicator:self.view];
            });
            
            self.checkinflag = false;
            [self.checkinBtn setTitle:@"CHECK IN" forState:UIControlStateNormal];
            
        };
        
        void(^failure)() = ^void(NSError* error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [QWActivityHelper removeActivityIndicator:self.view];
            });
            
            NSDictionary *errorInfo = [error userInfo];
            
            if ([UIAlertController class]){
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                               message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {}];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            } else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                                message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }

            
        };
        
        NSDictionary *dict = @{@"partnerid" : [self.data objectForKey:@"partnerid"],
                               @"mobile" : [self.data objectForKey:@"mobile"],
                               @"signature" : [self.data objectForKey:@"signature"],
                               @"transactionId": [self.data objectForKey:@"transactionId"],
                               @"outletid":self.outLetId,
                               @"retailerid":self.retailerid,
                               @"type":@"app"
                               };
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
        
        QWSdk *qw = [[QWSdk alloc] init];
        [qw checkout:data :success :failure];
    }
    
}


- (void) offerBtnPressed: (id) sender{
    GlobalSingleton *global = [GlobalSingleton sharedInstance];
    NSMutableArray *offersTemp = [NSMutableArray array];
    NSArray *offerIds = [self.currentPlace objectForKey:@"offers"];
    
    for (NSString *offerId in offerIds) {
        for (NSDictionary *offer in global.offers) {
            if ([[offer objectForKey:@"id"] isEqualToString: offerId]) {
                [offersTemp addObject:offer];
            }
        }
        
    }
    
    self.offers = offersTemp;
    
    if ([UIAlertController class]){
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Offers"
                                                                       message:@""
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        OffersAlertView *offersView = [[OffersAlertView alloc] init];
        offersView.offers = self.offers;
        offersView.navigation= self.navigationController;
        [alert setValue:offersView forKey:@"contentViewController"];
        offersView.preferredContentSize = CGSizeMake(272, 250);
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           NSLog(@"Cancel");
                                                           
                                                       }];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Offers"
                                                        message:@""
                                                       delegate:nil
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:nil, nil];
        
        OffersAlertView *offersView = [[OffersAlertView alloc] init];
        offersView.offers = self.offers;
        offersView.navigation = self.navigationController;
        [alert setValue:offersView forKey:@"contentViewController"];
        offersView.preferredContentSize = CGSizeMake(272, 250);
        
        [alert show];
        
    }
    
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    if ([self.amountText isFirstResponder]) {
        
        NSDictionary* info = [aNotification userInfo];
        CGRect keyPadFrame=[[UIApplication sharedApplication].keyWindow convertRect:[[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue] fromView:self.view];
        CGSize kbSize =keyPadFrame.size;
        
        CGRect activeRect=[self.view convertRect:self.amountText.frame fromView:self.amountText.superview];
        CGRect aRect = self.view.bounds;
        aRect.size.height -= (kbSize.height);
        
        CGPoint origin =  activeRect.origin;
        origin.y -= self.scrollView.contentOffset.y;
        
        if (!CGRectContainsPoint(aRect, origin)) {
            CGPoint scrollPoint = CGPointMake(0.0,CGRectGetMaxY(activeRect)-(aRect.size.height));
            [self.scrollView setContentOffset:scrollPoint animated:NO];
        }
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    CGRect frame = self.scrollView.frame;
    self.scrollView.frame = frame;
}

static inline BOOL IsEmpty(id thing) {
    return thing == nil
    || ([thing isEqual:[NSNull null]]) //JS addition for coredata
    || ([thing respondsToSelector:@selector(length)]
        && [(NSData *)thing length] == 0)
    || ([thing respondsToSelector:@selector(count)]
        && [(NSArray *)thing count] == 0);
}

- (NSString *)validateForm {
    NSString *errorMessage;
    
    if (![self.amountText.text  isValidAmount]){
        errorMessage = @"Please enter a valid amount";
    }
    
    return errorMessage;
}

- (void) menuBtnPressed: (id) sender {
    
    if (self.menuCount > 0) {
        
        NSMutableArray *photos = [[NSMutableArray alloc] init];
        NSMutableArray *photoUrls = [self.menuImages objectAtIndex: 0];
        
        for (NSURL *url in photoUrls) {
            IDMPhoto *photo = [IDMPhoto photoWithURL:url];
            [photos addObject:photo];
        }
        
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos];
        browser.displayActionButton = NO;
        [self presentViewController:browser animated:YES completion:nil];

    } else{
        if ([UIAlertController class]){
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:@"No menu found."
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"No menu found."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    }
}

-(void) callBtnPressed: (id) sender {
    
    if([self.currentPlace valueForKey:@"phone"]!=nil)
    {
        NSArray * phoneNumbers = [[NSArray alloc]init];
        phoneNumbers = [self.phonenumber componentsSeparatedByString:@","];
        NSString *phoneNumberwithoutspaces = [[phoneNumbers objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phoneNumberwithoutspaces];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    else
    {
        if ([UIAlertController class]){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:@"No Phone Number Available."
                                                                    preferredStyle:UIAlertControllerStyleActionSheet];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"No Phone Number Available."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    }
}

- (void) addressBtnPressed: (id) sender {
    
    NSString *msg;
    
    if ([[self.currentPlace objectForKey: @"delivery"] isEqualToString: @"Y"]) {
        msg = [NSString stringWithFormat:@"\n %@ \n\n Timings: %@ \n\n %@", [self.currentPlace objectForKey:@"address"], [self.currentPlace objectForKey:@"timings"], @"Only home delivery"];
    } else{
        msg = [NSString stringWithFormat:@"\n %@ \n\n Timings: %@", [self.currentPlace objectForKey:@"address"], [self.currentPlace objectForKey:@"timings"]];
    }
    
    if ([UIAlertController class]){
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Address"
                                                                       message:msg
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction* locateUs = [UIAlertAction actionWithTitle:@"Locate Us" style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * action){
                                                             
                                                             CLLocationCoordinate2D location = CLLocationCoordinate2DMake([[self.currentPlace objectForKey:@"latitude"] doubleValue],[[self.currentPlace objectForKey:@"longitude"] doubleValue]);
                                                             
                                                             MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:location addressDictionary:nil];
                                                             MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
                                                             item.name = [self.currentPlace objectForKey:@"retailername"];
                                                             [item openInMapsWithLaunchOptions:nil];
                                                             
                                                             
                                                         }];
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           NSLog(@"Cancel");
                                                           
                                                       }];
        
        
        [alert addAction:locateUs];
        [alert addAction:cancel];
        [self presentViewController:alert animated:YES completion:nil];
        
    }else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Address"
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:@"Locate Us", nil];
        [alert show];
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == [alertView cancelButtonIndex]){
        NSLog(@"Cancel");
    }else{
        
        if (alertView.tag == 999) {
            NSLog(@"BLA %@", [alertView textFieldAtIndex:0].text);
            [self issues:[alertView textFieldAtIndex:0].text];
        } else{
            CLLocationCoordinate2D location = CLLocationCoordinate2DMake([[self.currentPlace objectForKey:@"latitude"] doubleValue],[[self.currentPlace objectForKey:@"longitude"] doubleValue]);
            
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:location addressDictionary:nil];
            MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
            item.name = [self.currentPlace objectForKey:@"retailername"];
            [item openInMapsWithLaunchOptions:nil];
        }
    }
}


- (IBAction)quikpay:(id)sender {
    
    [QWActivityHelper displayActivityIndicator:self.view];
    
    NSString *errorMessage = [self validateForm];
    
    if(errorMessage)
    {
        [QWActivityHelper removeActivityIndicator:self.view];
        
        if ([UIAlertController class]){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:errorMessage
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message: errorMessage
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
    else
    {
        [QWActivityHelper removeActivityIndicator:self.view];
        QWPayNowViewController *pay = [[QWPayNowViewController alloc] init];
        
        [self.data setObject:self.amountText.text forKey:@"amount"];
        [self.data setObject:self.outLetId forKey:@"outletid"];
        
        pay.data = self.data;
        pay.presentationStyle = self.presentationStyle;
        [pay.data setObject:SDK_ACTION forKey:INTENDED_ACTION_FROM_SDK];
        [self.navigationController pushViewController:pay animated:YES];
    }
}

- (IBAction)reportIssues:(id)sender {
    
    if ([UIAlertController class]){
        
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Report Issue"
                                                                       message:@""
                                                                preferredStyle:UIAlertControllerStyleActionSheet];
        
        self.reportView = [[QWReportIssueViewController alloc] init];
        [alert setValue:self.reportView forKey:@"contentViewController"];
        
        self.reportView.preferredContentSize = CGSizeMake(272, 221);
        
        UIAlertAction* send = [UIAlertAction actionWithTitle:@"Send" style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action){
                                                         [self issues:self.reportView.message.text];
                                                         
                                                     }];
        
        UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * action) {
                                                           
                                                           NSLog(@"Cancel");
                                                           
                                                       }];
        
        
        [alert addAction: send];
        [alert addAction:cancel];
        
        [self presentViewController:alert animated:YES completion:nil];
        
    } else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Report Issue"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK", nil];
        alert.tag = 999;
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert show];
        

    }
}

- (void) issues: (NSString *) msg{
    [QWActivityHelper displayActivityIndicator:self.view];

    NSDictionary *dict = @{
                           @"message":msg,
                           @"outletid": self.outLetId,
                           @"partnerid" : [self.data objectForKey:@"partnerid"],
                           @"mobile" : [self.data objectForKey:@"mobile"],
                           @"signature" : [self.data objectForKey:@"signature"],
                           @"transactionId": [self.data objectForKey:@"transactionId"]
                           };
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:dict];
    
    void(^success)() = ^void(id  data){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [QWActivityHelper removeActivityIndicator:self.view];
        });
        
        if ([UIAlertController class]){
            
            UIAlertController* alertMessage = [UIAlertController alertControllerWithTitle:@""
                                                                                  message:@"Thanks for letting us know, we will get in touch with you to learn more about your experience. Have a great day!"
                                                                           preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* okAction = [UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * action) {
                                                                 NSLog(@"Dismiss");
                                                             }];
            
            
            [alertMessage addAction:okAction];
            
            [self presentViewController:alertMessage animated:YES completion:nil];
            
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"Thanks for letting us know, we will get in touch with you to learn more about your experience. Have a great day!"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    };
    
    void(^failure)() = ^void(NSError* error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [QWActivityHelper removeActivityIndicator:self.view];
        });
        
        NSDictionary *errorInfo = [error userInfo];
        
        if ([UIAlertController class]){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        
    };
    
    
    QWSdk *qw = [[QWSdk alloc] init];
    [qw reportIssue:data :success :failure];
}

@end

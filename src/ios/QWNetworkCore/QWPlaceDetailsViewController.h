//
//  QWPlaceDetailsViewController.h
//  QW_Network
//
//  Created by Monideep Purkayastha on 12/04/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QWReportIssueViewController.h"
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"

@interface QWPlaceDetailsViewController : UIViewController <UIAlertViewDelegate>

@property (strong,nonatomic) NSMutableDictionary *currentPlace;
@property (nonatomic, strong) NSMutableDictionary *data;
@property (strong, nonatomic) NSString *outLetId;
@property (strong, nonatomic) NSNumber *retailerid;
@property (strong, nonatomic) NSNumber *companyid;
@property (strong,nonatomic) NSString *phonenumber;
@property (strong,nonatomic) NSString *retailername;

@property (weak, nonatomic) IBOutlet UIImageView *defaultBanner;

@property (weak, nonatomic) IBOutlet UILabel *retailerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *outletNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *checkinBtn;
@property (weak, nonatomic) IBOutlet UITextField *amountText;
@property (weak, nonatomic) IBOutlet UIButton *quikpaybutton;

@property (weak, nonatomic) IBOutlet UIView *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *retailerLogo;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *amountView;
@property (weak, nonatomic) IBOutlet UILabel *menuLbl;
@property (weak, nonatomic) IBOutlet UILabel *callLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIImageView *logoImg;

- (IBAction)checkin:(id)sender;
- (IBAction)quikpay:(id)sender;

@property (nonatomic, strong) NSArray *photos;
@property (strong, nonatomic) NSArray *allViews;
@property (strong, nonatomic) NSArray *menuImages;
@property (nonatomic, strong) NSArray *uiViewArray;
@property (nonatomic,assign) NSInteger menuCount;
@property (nonatomic, assign) NSInteger offersCount;

@property (nonatomic,assign) Boolean checkinflag;
@property (weak, nonatomic) IBOutlet UIView *withoutMenuBtn;
@property (weak, nonatomic) IBOutlet UIView *withoutOffersBtn;

@property (weak, nonatomic) IBOutlet UIView *allActionBtns;
@property (weak, nonatomic) IBOutlet UIView *withoutMenuAndOfferBtn;

@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *callBtns;

@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *addressBtns;

@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *menuBtns;
@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *offerBtns;

@property (nonatomic, copy) NSMutableArray *offers;

@property (nonatomic, strong) QWReportIssueViewController *reportView;

@property (nonatomic, copy) NSString * presentationStyle;

- (IBAction)reportIssues:(id)sender;

@end

//
//  alertViewOfferCell.h
//  QuikWallet
//
//  Created by Monideep Purkayastha on 02/02/16.
//  Copyright © 2016 LivQuik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface alertViewOfferCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ldesc;
@property (weak, nonatomic) IBOutlet UILabel *tncLabel;

@end

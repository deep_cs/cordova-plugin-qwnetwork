//
//  QWPlacesContainer.m
//  QW_Network
//
//  Created by Monideep Purkayastha on 07/03/16.
//  Copyright © 2016 LivQuik Technology. All rights reserved.
//

#import "QWPlacesContainer.h"
#import "HMSegmentedControl.h"
#import "QWActivityHelper.h"
#import "QWPlacesViewController.h"
#import "GlobalSingleton.h"
#import <CoreLocation/CoreLocation.h>
#import "QWSdk.h"
#import "NotificationViewController.h"

@interface QWPlacesContainer ()

@end

NSArray *_pages;
HMSegmentedControl *segmentedControl;

@implementation QWPlacesContainer

- (id)init
{
    self = [super init];
    if (self != nil)
    {

    }
    return self;


}

- (void)viewDidLoad {
    [super viewDidLoad];

    if([UIViewController instancesRespondToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout=UIRectEdgeNone;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveFeedbackNotification:)
                                                 name:@"FeedbackNotification"
                                               object:nil];


    // Menu
    CGFloat viewWidth = CGRectGetWidth(self.view.frame);

    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"FOOD & DRINK", @"RETAIL"]];
    segmentedControl.frame = CGRectMake(0, 0, viewWidth , 50);

    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    segmentedControl.selectionIndicatorColor = [UIColor darkGrayColor];
    segmentedControl.selectionIndicatorHeight = 0.0;
    segmentedControl.backgroundColor = [UIColor colorWithRed:78.0/255.0 green:157.0/255.0 blue:196.0/255.0 alpha:1.0];
    segmentedControl.textColor = [UIColor colorWithRed:218.0/255.0 green:223.0/255.0 blue:225.0/255.0 alpha:1.0];
    segmentedControl.selectedTextColor = [UIColor whiteColor];
    segmentedControl.font = [UIFont fontWithName:@"HelveticaNeue" size:13.0];
    [segmentedControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentedControl];

    // Add a border
    UIView *border = [[UIView alloc]initWithFrame:CGRectMake(0, 50, viewWidth, 1)];
    [border setBackgroundColor:[UIColor colorWithRed:243.0/255.0 green:243.0/255.0 blue:243.0/255.0 alpha:1.0]];
    border.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:border];

    UIImage* image4 = [UIImage imageNamed:@"qwheader.png"];
    CGRect frameimg1 = CGRectMake(0, 0, 160, 20);
    UIButton *backButton = [[UIButton alloc] initWithFrame:frameimg1];
    backButton.contentMode = UIViewContentModeScaleAspectFit;
    backButton.userInteractionEnabled = NO;
    [backButton setBackgroundImage:image4 forState:UIControlStateNormal];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backButtonItem;

    [self setRightBarButtons:0];

    // Get Places
    [self getPlaces];

    // Get Notifications
    [self getNotifications];
}

- (void) getNotifications{
    // Display spinner
    [QWActivityHelper displayActivityIndicator:self.view];

    void(^success)() = ^void(id  data){
        [QWActivityHelper removeActivityIndicator:self.view];
        [self setRightBarButtons:[[data objectForKey:@"pendingpayments"] count]];
    };

    void(^failure)() = ^void(NSError* error) {
        NSDictionary *errorInfo = [error userInfo];
        NSLog(@"Failed %@", [errorInfo objectForKey:@"NSLocalizedDescription"]);
    };

    QWSdk *qw = [[QWSdk alloc] init];
    [qw pendingPayments:self.data :success :failure];
}

- (void) setRightBarButtons: (NSInteger) count{
    NSInteger notiCount = count;
    NSString *notistring = [NSString stringWithFormat:@"%ld",(long)notiCount];
    UIButton *Noti =  [UIButton buttonWithType:UIButtonTypeCustom];
    [Noti setImage:[UIImage imageNamed:@"notification.png"] forState:UIControlStateNormal];
    [Noti addTarget:self action:@selector(notification:)forControlEvents:UIControlEventTouchUpInside];
    [Noti setFrame:CGRectMake(0, 0, 33, 26)];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15, -5, 16, 16)];
    [label setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    label.layer.cornerRadius = 5;
    label.textAlignment = NSTextAlignmentCenter;
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_notification.png"]]];
    if (notiCount == 0) {
        label.hidden = YES;
    }else
    {
        label.text = notistring;
    }
    [Noti addSubview:label];
    UIBarButtonItem *btnNotification = [[UIBarButtonItem alloc] initWithCustomView:Noti];

    UIBarButtonItem *btnHome = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"Home.png"]  imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(popBack)];

    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:btnHome, btnNotification, nil]];

}


- (void) notification: (id) sender{
    NotificationViewController *notification = [[NotificationViewController alloc] init];
    notification.data = self.data;
    notification.loadingFlag = true;
    [self.navigationController pushViewController:notification animated:YES];
}

- (void) getPlaces{
    // Display spinner
    [QWActivityHelper displayActivityIndicator:self.view];

    void(^success)() = ^void(id  data){
        self.places = [self sortPlaces:data];
        self.offers = [data objectForKey:@"offers"];

        GlobalSingleton *global = [GlobalSingleton sharedInstance];
        global.offers = [NSArray arrayWithArray:[data objectForKey:@"offers"]];

        NSMutableArray *retailsTemp = [NSMutableArray array];
        NSMutableArray *fnbsTemp = [NSMutableArray array];

        for(NSDictionary *item in self.places) {
            if ([[item objectForKey: @"categoryid"] intValue] == 1) {
                [retailsTemp addObject: item];
            } else if ([[item objectForKey: @"categoryid"] intValue] == 3){
                [fnbsTemp addObject: item];
            }
        }

        self.retails = [NSArray arrayWithArray: retailsTemp];
        self.fnbs = [NSArray arrayWithArray: fnbsTemp];

        [self setupPageView];

        [QWActivityHelper removeActivityIndicator:self.view];
    };

    void(^failure)() = ^void(NSError* error) {
        [QWActivityHelper removeActivityIndicator:self.view];
        [self setupPageView];

        NSDictionary *errorInfo = [error userInfo];

        if ([UIAlertController class]){
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                           message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                                    preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];

            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
        } else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                            message:[errorInfo objectForKey:@"NSLocalizedDescription"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    };

    QWSdk *qw = [[QWSdk alloc] init];
    [qw getPlaces:self.data :success :failure];
}

- (void) popBack{
    [self dismissViewControllerAnimated:YES completion:nil];

    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"QWSdkDidTerminate_Notification"
     object:self];

}

- (NSMutableArray *)sortPlaces : (NSMutableDictionary *) responseData{
    // Sort and Update places
    NSMutableArray *newPlaceData = [NSMutableArray arrayWithArray: [responseData objectForKey:@"places"]];
    NSMutableArray *sortedPlaces = [[NSMutableArray alloc]init];

    double lat = [[self.data objectForKey:@"latitude"] doubleValue];
    double longi = [[self.data objectForKey:@"longitude"] doubleValue];

    CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:lat longitude:longi];

    for(NSMutableDictionary *place in newPlaceData){
        CLLocation *placeLocation = [[CLLocation alloc]initWithLatitude: [[place objectForKey:@"latitude"] doubleValue]
                                                              longitude:[[place objectForKey:@"longitude"] doubleValue]];

        CLLocationDistance distanceInMeters = [currentLocation distanceFromLocation:placeLocation]/1000;

        NSMutableDictionary *distances = [[NSMutableDictionary alloc] initWithDictionary:place];

        [distances setObject:[NSNumber numberWithFloat:distanceInMeters] forKey:@"distance"];

        [sortedPlaces addObject:distances];
    }

    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"distance"
                                                 ascending:YES];

    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [sortedPlaces sortedArrayUsingDescriptors:sortDescriptors];
    newPlaceData = [NSMutableArray arrayWithArray:sortedArray];

    return newPlaceData;
}

- (void) setupPageView{
    [self setupPages];

    self.pageView = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];

    self.pageView.dataSource = self;
    self.pageView.delegate = self;

    [[self.pageView view] setFrame:CGRectMake(0, 51, [[self view] bounds].size.width, [[self view] bounds].size.height + 37 - 51)];
    self.pageView.view.tag = 207;

    NSArray *viewControllers = [NSArray arrayWithObject:_pages[0]];
    [self.pageView setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];

    [self addChildViewController:self.pageView];
    [[self view] addSubview:[self.pageView view]];
    [self.pageView didMoveToParentViewController:self];
}

- (void) setupPages {

    QWPlacesViewController *places = [[QWPlacesViewController alloc] init];
    places.screenIndex = 0;
    places.places = self.fnbs;
    places.data = self.data;

    QWPlacesViewController *retails = [[QWPlacesViewController alloc] init];
    retails.screenIndex = 1;
    retails.places = self.retails;
    retails.data = self.data;

    _pages = [NSArray arrayWithObjects: places, retails, nil];
}

#pragma mark    UIPageViewController implementaion
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    if (nil == viewController) {
        return _pages[0];
    }
    NSInteger idx = [_pages indexOfObject:viewController];
    NSParameterAssert(idx != NSNotFound);
    if (idx >= [_pages count] - 1) {
        // we're at the end of the _pages array
        return nil;
    }
    // return the next page's view controller
    return _pages[idx + 1];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    if (nil == viewController) {
        return _pages[0];
    }
    NSInteger idx = [_pages indexOfObject:viewController];
    NSParameterAssert(idx != NSNotFound);
    if (idx <= 0) {
        // we're at the end of the _pages array
        return nil;
    }
    // return the previous page's view controller
    return _pages[idx - 1];
}

- (NSInteger) presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return  [_pages count];
}

- (NSInteger) presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

- (void) pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {

    UIViewController * currentView = [pageViewController.viewControllers objectAtIndex:0];

    NSUInteger index = [(QWPlacesViewController *) currentView screenIndex];
    [segmentedControl setSelectedSegmentIndex: index animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    UIViewController *initial =   _pages[segmentedControl.selectedSegmentIndex];
    NSArray *viewControllers = [NSArray arrayWithObject:initial];

    [self.pageView setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];

}

- (void) receiveFeedbackNotification:(NSNotification *) notification
{

    if ([[notification name] isEqualToString:@"FeedbackNotification"]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Thanks for your feedback."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }

}

@end
